## React Front End Task App
## Installation

- clone the projet from the git repository
- under your project directory execute this command

```
$ npm install
```

this will install all the required dependencies

- run your project using the following command

```
$ npm start
```

- your project will run on http://localhost:3000/
