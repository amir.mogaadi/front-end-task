import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Tree from './Tree'
import { items } from './test-data'

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      backgroundColor:"#eeeeee",
      
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

export default () => {
    const classes = useStyles();

    return (
       <div className={classes.root}>
           <Grid container spacing={2}>
                <Grid item xs={12}>
                 <Paper className={classes.paper}>HEADER</Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Paper>
                  <div className="grow">
                 
              </div>
              </Paper>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Paper>
                  <div className="grow">
                  <h2>Role : Qualité</h2>
                  <p>
                      Données accessible:
                  </p>
                    <Tree items={items}   />
                </div>
                  </Paper>
                </Grid>
            </Grid>
        </div>
    )
}

// { item => item.title }

// Label slot test code
// { item => `xx ${item.title} ${item.$open && item.$open || ''}` }


