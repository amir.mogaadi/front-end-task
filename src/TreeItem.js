import React from 'react'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
export default function Item({
    items,
    style,
    toggleOpen,
    labelSlot,
    prependSlot,
    
}) {
    // console.log('update', new Date().getTime())
    const subTree = items.map(item => (
        <div key={item.id} style={style}>
            <span className="expander">
           
                {item.items 
                 
                    ? item.$open
                    
                        ? <ExpandMoreIcon/>
                    
                        : <ChevronRightIcon/>
                    : null}
                  
            </span>
            {prependSlot(item)}
            {labelSlot(item)}
            {item.items &&
                (item.$open && (
                    <Item
                        {...{
                            style: { ...style, marginLeft: '20px' },
                            items: item.items,
                            toggleOpen,
                            labelSlot,
                            prependSlot,
                           
                        }}
                    />
                ))} 
        </div>
    ))

    return subTree
}
